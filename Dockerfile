FROM node:10.16.0-alpine
# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json package-lock.json ./

#xRUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . ./

RUN npm install && npm run build
