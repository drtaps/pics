## Rendu de Alexis Legrain

Les différentes étapes : 
* Créer les différents environnement sur Heroku 
* Créer les différentes branches au sein du git 
* Création du .gitlab-ci.yml pour mettre en relation les environnement et branches pour l'IC


Pour mettre en place le CI sur les différents environnement, j'ai tout d'abord, décidé de tester le déplyement sur l'environnemet de
développement.

Une fois la mise en place sur l'environnement de développement, j'ai décidé de mettre en place l'IC sur recette et production.

Pour cela, j'ai suivi le tuto suivant : https://medium.com/@seulkiro/deploy-node-js-app-with-gitlab-ci-cd-214d12bfeeb5

Au sein du .gitlab-ci.yml, on précise pour chaque branche sur quelle environnement déployer l'application lors d'un push.
* branch recette -> recette 
* branch develop -> develop 
* branch master -> production 



Le deplyoement avec de la dockerisation ne marche pas mais le Dockerfile fonctionne